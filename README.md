# Principe

Deux jeux de données : classification et régression

Proposer le meilleur prédicteur

Évaluer la qualité :
	- Classifieur : probabilité d'erreur
	- Régression : espérance de l'erreur quadratique

# Note

- 1/3 : rigueur et méthodologie

- 1/3 : performances obtenues sur les prédicteurs

- 1/3 : qualité du rendu écrit

# Rendu

	- Rapport RStudio Notebook : 8 pages français
	- Fichier R avec 2 fonctions
	- Fichier RData avec l'environnement

http://maggle.gi.utc/ et Moodle
Groupe : D1_P1_G

# Démarche

## Classifieurs

	- knn
	- glm(lfp ~. ,data=Participation,family=binomial)
	- LDA/QDA/NB

Sélection de modèle : pROC + sélection de variable (à chercher)

## Regresseurs

(transformations de données)

	- knn.reg
	- lm

Sélection de modèle : AIC, AIC

